import numpy as np
import scipy.linalg as spla
from scipy.special import gamma, kv

from myjive.names import GlobNames as gn
from names import GPGlobNames as gpgn
from myjive.model import Model
from myjive.solver import Constrainer
from myjive.util.proputils import check_dict

OBSNOISE = 'obsNoise'
PDNOISE = 'pdNoise'
BCNOISE = 'bcNoise'
PRIOR = 'prior'
TYPE = 'type'
FUNC = 'func'
HYPERPARAMS = 'hyperparams'
BOUNDARY = 'boundary'
GROUPS = 'groups'
DOFS = 'dofs'
MEANS = 'means'
COVS = 'covs'
SHAPE = 'shape'
INTSCHEME = 'intScheme'


class GPModel(Model):
    def CONFIGUREFEM(self, globdat):
        self._configure_fem(globdat)

    def CONFIGUREPRIOR(self, globdat):
        self._configure_prior(globdat)

    def GETPRIORMEAN(self, globdat):
        m_prior, field = self._get_prior_mean(globdat)
        return m_prior, field

    def GETPRIORCOVARIANCE(self, globdat):
        Sigma_post, field = self._get_prior_covariance(globdat)
        return Sigma_post, field

    def GETPOSTERIORMEAN(self, globdat):
        m_post, field = self._get_posterior_mean(globdat)
        return m_post, field

    def GETPOSTERIORCOVARIANCE(self, globdat):
        Sigma_post, field = self._get_posterior_covariance(globdat)
        return Sigma_post, field

    def GETPRIORSAMPLES(self, globdat, **kwargs):
        samples, field = self._get_prior_samples(globdat, **kwargs)
        return samples, field

    def GETPOSTERIORSAMPLES(self, globdat, **kwargs):
        samples, field = self._get_posterior_samples(globdat, **kwargs)
        return samples, field

    def KALMANUPDATE(self, prior_samples, globdat, **kwargs):
        samples, field = self._kalman_update(prior_samples, globdat, **kwargs)
        return samples, field

    def GETLOGLIKELIHOOD(self, globdat):
        l = self._get_log_likelihood(globdat)
        return l

    def PROJECTSAMPLES(self, prior_samples, post_samples, globdat):
        return self._project_samples(prior_samples, post_samples, globdat)

    @Model.save_config
    def configure(self, globdat, *, shape, obsNoise, pdNoise=1e-8, bcNoise=1e-8, prior, boundary={TYPE:"dirichlet"}):
        # Validate input arguments
        check_dict(self, shape, [TYPE, INTSCHEME])
        check_dict(self, prior, [TYPE, FUNC, HYPERPARAMS])
        check_dict(self, boundary, [TYPE])
        shapeprops = shape
        self._noise2 = obsNoise**2
        self._pdnoise2 = pdNoise**2
        self._bcnoise2 = bcNoise**2
        priorprops = prior
        bcprops = boundary

        self._dc = globdat[gn.DOFSPACE].dof_count()
        self._shape = globdat[gn.SHAPEFACTORY].get_shape(shapeprops[TYPE], shapeprops[INTSCHEME])
        self._rank = self._shape.global_rank()

        self._prior = priorprops[TYPE]
        if self._prior == 'kernel':
            self._kernel = priorprops[FUNC]
        elif self._prior == 'SPDE':
            self._covariance = priorprops[FUNC]
        else:
            raise ValueError('prior has to be "kernel" or "SPDE"')

        self._hyperparams = {}
        for key, value in priorprops[HYPERPARAMS].items():
            if value != 'opt':
                self._hyperparams[key] = float(value)
            else:
                self._hyperparams[key] = 'opt'

        # Get the type of BC enforcement
        self._bctype = bcprops[TYPE]
        if self._bctype not in ['direct', 'dirichlet']:
            raise ValueError('boundary has to be "dirichlet" or "direct"')
        self._bcgroups = bcprops.get(GROUPS,[])
        self._bcdofs = bcprops.get(DOFS, [])
        self._bcmeans = bcprops.get(MEANS, [])
        self._bccovs = bcprops.get(COVS,[])

        # Get the dofs of the fine mesh
        self._dof_types = globdat[gn.DOFSPACE].get_types()

        # Add the dofs to the coarse mesh
        for doftype in self._dof_types:
            globdat[gpgn.COARSEMESH][gn.DOFSPACE].add_type(doftype)
            for node in range(len(globdat[gpgn.COARSEMESH][gn.NSET])):
                globdat[gpgn.COARSEMESH][gn.DOFSPACE].add_dof(node, doftype)

        # Get the number of observations (which is the number of dofs in the coarse mesh)
        self._nobs = globdat[gpgn.COARSEMESH][gn.DOFSPACE].dof_count()

        # Get the phi matrix
        self._Phi = self._get_phi(globdat)

        # Store Phi in globdat
        globdat['Phi'] = self._Phi

    def _configure_fem(self, globdat):

        # Get K, M and f from globdat
        K = globdat.get(gn.MATRIX0)
        M = globdat.get(gn.MATRIX2)
        f = globdat.get(gn.EXTFORCE)
        c = globdat.get(gn.CONSTRAINTS)

        conmanK = Constrainer(c, K)
        conmanM = Constrainer(c, M)

        # Get the actual constrained stiffness matrix and force vector
        self._Mc = conmanM.get_output_matrix()
        self._Kc = conmanK.get_output_matrix()
        self._K = conmanK.get_input_matrix()
        self._f = f
        fc = conmanK.get_rhs(f)

        # Get the constraints
        self._cdofs, self._cvals = c.get_constraints()

        # Constrain the phi matrix based on Dirichlet BCs
        self._Phic = self._constrain_phi(self._Phi, self._cdofs)

        # Store Phic in globdat
        globdat['Phic'] = self._Phic

        # Get the observation operator
        self._H = self._Phic.T @ self._Kc

        # Get the observed force vector
        self._g = self._Phic.T @ fc

    def _configure_prior(self, globdat):

        # Define a dictionary with relevant functions
        eval_dict = self._get_eval_dict(globdat)

        # Check if we have a kernel covariance
        if self._prior == 'kernel':

            # Get the covariance matrix by looping over all dofs
            self._Sigma = np.zeros((self._dc, self._dc))

            nodes = globdat[gn.NSET]
            dofspace = globdat[gn.DOFSPACE]

            for i in range(len(nodes)):
                icoords = nodes[i].get_coords()
                idofs = dofspace.get_dofs([i], dofspace.get_types())
                eval_dict['x0'] = icoords

                for j in range(len(nodes)):
                    jcoords = globdat[gn.NSET][j].get_coords()
                    jdofs = dofspace.get_dofs([j], dofspace.get_types())
                    eval_dict['x1'] = jcoords

                    self._Sigma[np.ix_(idofs, jdofs)] = eval(self._kernel, eval_dict)

        else:

            # Get the covariance matrix by 1 matrix evaluation
            self._Sigma = eval(self._covariance, eval_dict)

        # Apply boundary conditions to the prior
        self._m, self._Sigma = self._apply_covariance_bcs(self._Sigma, globdat)

        # Get the centered observation vector (as a deviation from the prior mean)
        self._y = self._g - self._H @ self._m

    def _get_prior_mean(self, globdat):

        # Return the prior of the force field
        return self._m, gn.STATE0

    def _get_posterior_mean(self,  globdat):

        ##################
        # POSTERIOR MEAN #
        ##################

        # u_bar = Sigma * H.T * inv(H * Sigma * H.T + Sigma_e) * y

        # Get the relevant matrices
        self._get_sqrtObs()
        self._get_v0()

        # Get the posterior of the force field
        if not '_u_post' in vars(self):
            self._u_post = self._m + self._premul_Sigma(self._H.T @ self._solve_triangular(self._sqrtObs.T, self._v0, lower=False))

        # Return the posterior of the displacement field
        return self._u_post, gn.STATE0

    def _get_prior_covariance(self, globdat):

        ####################
        # PRIOR COVARIANCE #
        ####################

        # Compute the full covariance matrix
        return self._Sigma, gn.STATE0

    def _get_posterior_covariance(self, globdat):

        ########################
        # POSTERIOR COVARIANCE #
        ########################

        # Sigma_bar = Sigma - Sigma * H.T * inv(H * Sigma * H.T + Sigma_e) * H * Sigma
        Sigma_prior, _ = self._get_prior_covariance(globdat)

        # Get the relevant matrices
        self._get_V1()

        # Compute the full covariance matrix
        Sigma_post = Sigma_prior.copy()
        Sigma_post -= self._V1.T @ self._V1
        return Sigma_post, gn.STATE0

    def _get_prior_samples(self, globdat, nsamples=1, rng=np.random.default_rng()):
        dc = globdat[gn.DOFSPACE].dof_count()
        samples = np.zeros((dc, nsamples))

        #################
        # PRIOR SAMPLES #
        #################

        # u = m + sqrt(Sigma) * z

        # Get the relevant matrices
        self._get_sqrtSigma()

        # Get a loop to create all samples
        for i in range(nsamples):

            # Get a sample from the standard normal distribution
            # NOTE: self._sqrtSigma.shape[1] does not have to be the same as self._dc
            # For example, if Ensemble Kalman is used, it is equal to self._nens instead.
            z = rng.standard_normal(self._sqrtSigma.shape[1])

            # Get the sample of the force field
            u = self._sqrtSigma @ z + self._m

            # Add the sample to the table
            samples[:,i] = u

        # Return the array of samples
        return samples, gn.STATE0

    def _get_posterior_samples(self, globdat, nsamples=1, rng=np.random.default_rng()):
        dc = globdat[gn.DOFSPACE].dof_count()
        samples = np.zeros((dc, nsamples))

        #####################
        # POSTERIOR SAMPLES #
        #####################

        # u = m + sqrt(Sigma) * z1 + Sigma * H.T * inv(H * Sigma * H + Sigma_e) * (y - H * sqrt(Sigma) * z1 + sqrt(Sigma_e) * z2)

        # Get the relevant matrices
        self._get_sqrtObs()
        self._get_sqrtSigma()
        self._get_sqrtNoise()

        # Get a loop to create all samples
        for i in range(nsamples):

            # Get two samples from the standard normal distribution
            # Get a sample from the standard normal distribution
            # NOTE: self._sqrtSigma.shape[1] does not have to be the same as self._dc
            # For example, if Ensemble Kalman is used, it is equal to self._nens instead.
            z1 = rng.standard_normal(self._sqrtSigma.shape[1])
            z2 = rng.standard_normal(self._nobs)

            # Get x1 ~ N(0, alpha^2 M) and x2 ~ N(0, Sigma_e)
            x1 = self._sqrtSigma @ z1
            x2 = self._sqrtNoise @ z2

            # Compute the perturbed observation
            u_pert = self._y - self._H @ x1 + x2

            # Multiply the perturbed observation by the Kalman gain
            u = self._solve_triangular(self._sqrtObs, u_pert, lower=True)
            u = self._solve_triangular(self._sqrtObs.T, u, lower=False)
            u = self._premul_Sigma(self._H.T @ u)

            # Add the prior sample and mean
            u += x1 + self._m

            # Add the sample to the table
            samples[:,i] = u

        # Return the array of samples
        samples = gn.STATE0

    def _kalman_update(self, samples_prior, globdat, rng=np.random.default_rng()):
        samples = np.zeros_like(samples_prior)

        #####################
        # POSTERIOR SAMPLES #
        #####################

        # u = m + sqrt(Sigma) * z1 + Sigma * H.T * inv(H * Sigma * H + Sigma_e) * (y - H * sqrt(Sigma) * z1 + sqrt(Sigma_e) * z2)

        # Get the relevant matrices
        self._get_sqrtObs()
        self._get_sqrtNoise()

        # Get a loop to create all samples
        for i, u_prior in enumerate(samples_prior.T):

            # Center the prior sample
            u_centered = u_prior - self._m

            # Get x ~ N(0, Sigma_e)
            z = rng.standard_normal(self._nobs)
            x = self._sqrtNoise @ z

            # Compute the perturbed observation
            u_pert = self._y - self._H @ u_centered + x

            # Multiply the perturbed observation by the Kalman gain
            tmp = self._solve_triangular(self._sqrtObs, u_pert, lower=True)
            tmp = self._solve_triangular(self._sqrtObs.T, tmp, lower=False)
            u_post = self._premul_Sigma(self._H.T @ tmp)

            # Add the prior sample and mean
            u_post += u_centered + self._m

            # Add the sample to the table
            samples[:,i] = u_post

        # Return the array of samples
        return samples, gn.STATE0

    def _project_samples(self, prior_samples, post_samples, globdat):

        # Get the projection matrix
        self._get_P()

        # Project the prior samples
        return self._P @ prior_samples, self._P @ post_samples

    def _get_log_likelihood(self, globdat):

        ##################
        # LOG LIKELIHOOD #
        ##################

        # l = - 1/2 * y * inv(H * Sigma * H.T + Sigma_e) * y - 1/2 * log|H * Sigma * H.T + Sigma_e| - n/2 * log(2*pi)

        # Get the relevant matrices
        self._get_sqrtObs()
        self._get_v0()

        l = - 0.5 * self._v0.T @ self._v0 - np.sum(np.log(self._sqrtObs.diagonal())) - 0.5 * self._nobs * np.log(2*np.pi)

        return l

    def _get_eval_dict(self, globdat):

        # Define a dictionary with relevant functions
        eval_dict = {'inv':np.linalg.inv, 'exp':np.exp, 'norm':np.linalg.norm, 'gamma':gamma, 'kv':kv, 'np':np}
        eval_dict.update(self._hyperparams)

        # Check if we have an SPDE covariance
        if self._prior == 'SPDE':

            nodes = globdat[gn.NSET]
            dofspace = globdat[gn.DOFSPACE]

            if self._rank >= 1:
                x = np.zeros(self._dc)
            if self._rank >= 2:
                y = np.zeros(self._dc)

            for i in range(len(nodes)):
                icoords = nodes[i].get_coords()
                idofs = dofspace.get_dofs([i], dofspace.get_types())

                if self._rank >= 1:
                    x[idofs] = icoords[0]
                if self._rank >= 2:
                    y[idofs] = icoords[1]

            if self._rank >= 1:
                eval_dict['x'] = x
            if self._rank >= 2:
                eval_dict['y'] = y

            g = self._Phi @ np.linalg.solve(self._Phi.T @ self._Phi, self._g)

            # Add the mass and stiffness matrices to the dictionary
            eval_dict['M'] = self._Mc.toarray()
            eval_dict['K'] = self._Kc.toarray()
            eval_dict['F'] = np.outer(self._f, self._f)
            eval_dict['G'] = np.outer(g, g)
            eval_dict['Phi'] = self._Phi

        return eval_dict

    def _get_phi(self, globdat):

        elemsc = globdat[gpgn.COARSEMESH][gn.ESET]
        nodes = globdat[gn.NSET]
        nodesc = globdat[gpgn.COARSEMESH][gn.NSET]
        dofs = globdat[gn.DOFSPACE]
        dofsc = globdat[gpgn.COARSEMESH][gn.DOFSPACE]

        Phi = np.zeros((dofs.dof_count(), dofsc.dof_count()))

        # Go over the coarse mesh
        for elemc in elemsc:
            inodesc = elemc.get_nodes()
            coordsc = nodesc.get_some_coords(inodesc)

            # Get the bounding box of the coarse element
            bbox = np.zeros((self._rank, 2))
            for i in range(self._rank):
                bbox[i,0] = min(coordsc[i,:])
                bbox[i,1] = max(coordsc[i,:])

            # Go over the fine mesh
            for inode, node in enumerate(nodes):
                coords = node.get_coords()

                # Check if the node falls inside the bounding box
                inside = True
                for i in range(self._rank):
                    if coords[i] < bbox[i,0] or coords[i] > bbox[i,1]:
                        inside = False
                        break

                # If so, check if the node falls inside the shape itself
                if inside:
                    loc_point = self._shape.get_local_point(coords, coordsc)
                    inside = self._shape.contains_local_point(loc_point, tol=1e-8)

                # If so, add the relative shape function values to the Phi matrix
                if inside:
                    svals = np.round(self._shape.eval_shape_functions(loc_point), 12)
                    idofs = dofs.get_dofs([inode], self._dof_types)

                    for i, inodec in enumerate(inodesc):
                        sval = svals[i]
                        idofsc = dofsc.get_dofs([inodec], self._dof_types)
                        Phi[idofs, idofsc] = sval

        return Phi

    def _constrain_phi(self, Phi, cdofs):
        Phic = Phi.copy()

        for i in range(Phic.shape[1]):
            for cdof in cdofs:
                if np.isclose(Phic[cdof,i], 1):
                    Phic[:,i] = 0.0
                    Phic[cdof,:] = 0.0

        return Phic

    def _apply_covariance_bcs(self, Sigma, globdat):

        Sigmac = Sigma.copy()
        mc = np.zeros(self._dc)

        # Add a tiny noise to ensure Sigma is positive definite rather than semidefinite
        Sigmac += self._pdnoise2 * np.identity(self._dc)

        # Get the internal node indices
        idofs = np.delete(np.arange(self._dc), self._cdofs)

        # Check if the boundary condition should be applied directly or via dirichlet BCs
        if self._bctype == 'direct':

            # Split Sigma along boundary and internal nodes
            Sigma_bb = Sigmac[np.ix_(self._cdofs, self._cdofs)]
            Sigma_bi = Sigmac[np.ix_(self._cdofs, idofs)]
            Sigma_ib = Sigmac[np.ix_(idofs, self._cdofs)]

            Sigma_bb_inv = np.linalg.inv(Sigma_bb)

            # Update the prior mean by observing the displacement at the bcs
            mc[idofs] += Sigma_ib @ (Sigma_bb_inv @ self._cvals)
            mc[self._cdofs] = self._cvals

            # Update the prior covariance as well
            Sigmac[np.ix_(idofs,idofs)] -= Sigma_ib @ Sigma_bb_inv @ Sigma_bi

            # Decouple the bc covariance from the internal nodes
            Sigmac[self._cdofs,:] = Sigmac[:,self._cdofs] = 0.0

        elif self._bctype == 'dirichlet':

            # Split K along boundary and internal nodes
            K_ib = -self._K.toarray()[:,self._cdofs]
            K_ib[self._cdofs] = np.identity(len(self._cdofs))

            # Decouple the bc covariance from the internal nodes
            Sigmac[self._cdofs,:] *= 0.0
            Sigmac[:,self._cdofs] *= 0.0

            # Get a matrix that defines the constraint equations
            conmat = np.zeros((self._dc, len(self._bcgroups)))
            ds = globdat[gn.DOFSPACE]
            for i, (group, dof) in enumerate(zip(self._bcgroups, self._bcdofs)):
                idofs = ds.get_dofs(globdat[gn.NGROUPS][group], [dof])
                conmat[idofs, i] = 1
            conmat = conmat[self._cdofs,:]

            # Get the boundary mean vector
            meanvec = np.array(self._bcmeans)
            mean_bc = conmat @ meanvec

            # Add the boundary mean vector to the prior
            mc += np.linalg.solve(self._Kc.toarray(), K_ib @ mean_bc)

            # Get the boundary covariance matrix
            sqrtcovmat = np.diag(self._bccovs)
            sqrtSigma_bc = conmat @ sqrtcovmat

            # Recouple the internal nodes based on the boundary covariance matrix
            sqrtSigma_mod = np.linalg.solve(self._Kc.toarray(), K_ib @ sqrtSigma_bc)
            Sigmac += sqrtSigma_mod @ sqrtSigma_mod.T

        else:
            raise ValueError('boundary has to be "dirichlet" or "direct"')

        # Add separate boundary noise to ensure positive definiteness
        Sigmac[self._cdofs,self._cdofs] += self._bcnoise2

        return mc, Sigmac

    def _solve_triangular(self, A, b, lower):
        return spla.solve_triangular(A, b, lower=lower)

    def _get_Sigma_obs(self):

        if not '_Sigma_obs' in vars(self):
            self._Sigma_obs = self._H @ self._Sigma @ self._H.T + np.identity(self._nobs) * self._noise2

    def _get_sqrtObs(self):

        if not '_sqrtObs' in vars(self):
            self._get_Sigma_obs()
            self._sqrtObs = np.linalg.cholesky(self._Sigma_obs)

    def _get_sqrtSigma(self):

        if not '_sqrtSigma' in vars(self):
            self._sqrtSigma = np.linalg.cholesky(self._Sigma)

    def _get_sqrtNoise(self):

        if not '_sqrtNoise' in vars(self):
            self._sqrtNoise = np.sqrt(self._noise2) * np.identity(self._nobs)

    def _get_v0(self):

        if not '_v0' in vars(self):
            self._get_sqrtObs()
            self._v0 = self._solve_triangular(self._sqrtObs, self._y, lower=True)

    def _get_V1(self):

        if not '_V1' in vars(self):
            self._get_sqrtObs()
            self._V1 = self._postmul_Sigma(self._solve_triangular(self._sqrtObs, self._H, lower=True))

    def _get_P(self):

        if not '_P' in vars(self):
            self._P = self._Phi @ np.linalg.inv(self._Phi.T @ self._Phi) @ self._Phi.T

    def _premul_Sigma(self, X):

        return self._Sigma @ X

    def _postmul_Sigma(self, X):

        return X @ self._Sigma
