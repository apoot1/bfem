#!/bin/bash
echo "Activating base environment"
eval "$(conda shell.bash hook)"
conda activate base

message(){
	let "n = ${#1} + 4"
	echo ""
	for i in $(seq $n); do echo -n "#"; done
	echo ""
	echo -n "# "
	echo -n "$1"
	echo -n " #"
	echo ""
	for i in $(seq $n); do echo -n "#"; done
	echo ""
	echo ""
}

build_bfem(){
	message "CREATING bfem ENVIRONMENT"
	conda env create -f ../ENVIRONMENT.yml

	message "ADDING LOCAL PATHS"
	conda activate bfem
	conda develop ~/Storage/git/bfem
	conda deactivate
}

build_bfem_dev(){
	message "CREATING bfem-dev ENVIRONMENT"
	conda env create -f ENVIRONMENT-dev.yml -y

	message "UPDATING bfem ENVIRONMENT"
	conda env update --name bfem-dev --file ~/Storage/git/myjive/ENVIRONMENT.yml

	message "ADDING LOCAL PATHS"
	conda activate bfem-dev
	conda develop ~/Storage/git/bfem
	conda develop ~/Storage/git/myjive
	conda deactivate
}

# (re)build bfem environment
if conda env list | grep -q "^bfem "; then
	while true; do
		read -p "bfem environment already exists
Do you want to rebuild it? [Y/n] " yn
		case $yn in
			[Yy]* )
				message "REMOVING bfem ENVIRONMENT"
				conda remove --name bfem --all -y
				build_bfem
				break
				;;
			[Nn]* )
				echo "Skipping bfem build"
				break
				;;
			* )
				echo "Please answer yes or no."
				;;
		esac
	done
else
	build_bfem
fi


# (re)build bfem-dev environment
if conda env list | grep -q "^bfem-dev "; then
	while true; do
		read -p "bfem-dev environment already exists.
Do you want to rebuild it? [Y/n] " yn
		case $yn in
			[Yy]* )
				message "REMOVING bfem-dev ENVIRONMENT"
				conda remove --name bfem-dev --all -y
				build_bfem_dev
				break
				;;
			[Nn]* )
				echo "Skipping bfem-dev build"
				break
				;;
			* )
				echo "Please answer yes or no."
				;;
		esac
	done
else
	build_bfem_dev
fi

