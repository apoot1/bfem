
class GPGlobNames:
    COARSEMESH = "coarseMesh"
    GP = "gp"
    PRIOR = "prior"
    POSTERIOR = "posterior"
    MEAN = "mean"
    COVARIANCE = "covariance"
    STD = "std"
    LOGLIKELIHOOD = "logLikelihood"
    SAMPLES = "samples"
