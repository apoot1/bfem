import numpy as np
from myjive.app import main
import myjive.util.proputils as pu
from declare import declare_extra_models, declare_extra_modules
from myjivex.util import QuickViewer
from myjivex import declare_all as declarex
from copy import deepcopy

props = pu.parse_file('plate.pro')
props['model']['gp']['prior']['func'] = 'K'
props['model']['gp']['prior']['hyperparams'] = {}

props_c = {}
props_c['init'] = deepcopy(props['gpinit'])
props_c['init']['type'] = 'Init'
props_c['solver'] = deepcopy(props['gpsolver'])
props_c['solver']['type'] = 'Linsolve'
props_c['solver'].pop("nsample")
props_c['solver'].pop("seed")
props_c['model'] = deepcopy(props['model'])
props_c['model']['models'] = [ "solid", "load", "diri" ]
props_c['init']['mesh']['file'] = 'meshes/plate_r0.msh'

extra_declares = [declarex, declare_extra_models, declare_extra_modules]
globdat_c = main.jive(props_c, extra_declares=extra_declares)
u_coarse = globdat_c['state0']

globdat = main.jive(props, extra_declares=extra_declares)
u = globdat['state0']
f = globdat['extForce']

mean = globdat['gp']['mean']
u_post = mean['posterior']['state0']
samples = globdat['gp']['samples']
samples_u_prior = samples['prior']['state0']
samples_f_prior = samples['prior']['extForce']
samples_u_post = samples['posterior']['state0']
samples_f_post = samples['posterior']['extForce']

std = globdat['gp']['std']
std_u_post = std['posterior']['state0']

cov = globdat['gp']['covariance']
cov_u_post = cov['posterior']['state0']

Phi = globdat['Phi']

QuickViewer(u_post, globdat, comp=0, dpi=600, figsize=(7.5,3), fname='img/K/mean_state0-x_posterior.png')
QuickViewer(std_u_post, globdat, comp=0, dpi=600, figsize=(7.5,3), fname='img/K/std_state0-x_posterior.png')

dc = len(u)
pdNoise = 1e-4

cov_u_post += pdNoise**2 * np.identity(dc)

l, Q = np.linalg.eigh(cov_u_post)

newl = l * abs(Q.T @ f)
newcov = Q @ np.diag(newl) @ Q.T
newvar = newcov.diagonal()
newstd = np.sqrt(newvar)

QuickViewer(cov_u_post @ f, globdat, comp=0, dpi=600, figsize=(7.5,3), fname='img/K/std_state0-x_error_recovered.png')
QuickViewer(newstd, globdat, comp=0, dpi=600, figsize=(7.5,3), fname='img/K/std_state0-x_posterior_rescaled.png')
