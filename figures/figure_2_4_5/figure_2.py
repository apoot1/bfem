from myjive.app import main
import myjive.util.proputils as pu
from declare import declare_extra_models, declare_extra_modules
from myjivex.util import QuickViewer
from myjivex import declare_all as declarex
from copy import deepcopy

props = pu.parse_file('plate.pro')

props_c = {}
props_c['init'] = deepcopy(props['gpinit'])
props_c['init']['type'] = 'Init'
props_c['solver'] = deepcopy(props['gpsolver'])
props_c['solver']['type'] = 'Linsolve'
props_c['solver'].pop("nsample")
props_c['solver'].pop("seed")
props_c['model'] = deepcopy(props['model'])
props_c['model']['models'] = [ "solid", "load", "diri" ]
props_c['init']['mesh']['file'] = 'meshes/plate_r0.msh'

extra_declares = [declarex, declare_extra_models, declare_extra_modules]
globdat_c = main.jive(props_c, extra_declares=extra_declares)
u_coarse = globdat_c['state0']

globdat = main.jive(props, extra_declares=extra_declares)
u = globdat['state0']
Phi = globdat['Phi']

err = u - Phi @ u_coarse

QuickViewer(u_coarse, globdat_c, comp=0, dpi=600, figsize=(7.5,3), fname='img/core-plots/state0-x_coarse.png')
QuickViewer(u, globdat, comp=0, dpi=600, figsize=(7.5,3), fname='img/core-plots/state0-x.png')
QuickViewer(err, globdat, comp=0, dpi=600, figsize=(7.5,3), fname='img/core-plots/error_state0-x.png')
