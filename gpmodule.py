import numpy as np

from myjive.names import GlobNames as gn
from names import GPGlobNames as gpgn

from myjive.implicit import LinsolveModule
from myjive.util import Table, to_xtable

TYPE = 'type'
GETUNITMASSMATRIX = 'getUnitMassMatrix'
GETSTRAINMATRIX = 'getStrainMatrix'
EXPLICITINVERSE = 'explicitInverse'
POSTPROJECT = 'postproject'
NSAMPLE = 'nsample'
SEED = 'seed'

class GPModule(LinsolveModule):
    @LinsolveModule.save_config
    def configure(self, globdat, *, getUnitMassMatrix=True, getStrainMatrix=True, explicitInverse=True, postProject=False, nsample=1, seed=None, **otherprops):
        super().configure(globdat, getStrainMatrix=getStrainMatrix, **otherprops)

        # Validate input arguments
        self._get_unit_mass_matrix = getUnitMassMatrix
        self._get_strain_matrix = getStrainMatrix
        self._explicit_inverse = explicitInverse
        self._postproject = postProject
        self._nsample = nsample
        self._seed = seed

        self._models = globdat[gn.MODELS]

    def init(self, globdat):
        pass

    def run(self, globdat):

        # Run solvermodule first
        output = super().run(globdat)

        # Check if we should invert K explicitly
        if self._explicit_inverse:
            self._Kinv = np.linalg.inv(self._solver.get_matrix().toarray())

        # Optionally get the mass matrix
        if self._get_unit_mass_matrix:
            M = self._get_empty_matrix(globdat)

            for model in self.get_relevant_models("GETMATRIX2", self._models):
                M = model.GETMATRIX2(M, globdat, unit_matrix=True)

            # Store mass matrix in Globdat
            globdat[gn.MATRIX2] = M

        # Configure the GP based on the fine FEM results
        for model in self.get_relevant_models("CONFIGUREFEM", self._models):
            model.CONFIGUREFEM(globdat)
        for model in self.get_relevant_models("CONFIGUREPRIOR", self._models):
            model.CONFIGUREPRIOR(globdat)

        # Create a dictionary for the gp output
        globdat[gpgn.GP] = {}
        globdat[gpgn.GP][gpgn.MEAN] = {}
        globdat[gpgn.GP][gpgn.MEAN][gpgn.PRIOR] = {}
        globdat[gpgn.GP][gpgn.MEAN][gpgn.POSTERIOR] = {}
        globdat[gpgn.GP][gpgn.COVARIANCE] = {}
        globdat[gpgn.GP][gpgn.COVARIANCE][gpgn.PRIOR] = {}
        globdat[gpgn.GP][gpgn.COVARIANCE][gpgn.POSTERIOR] = {}
        globdat[gpgn.GP][gpgn.STD] = {}
        globdat[gpgn.GP][gpgn.STD][gpgn.PRIOR] = {}
        globdat[gpgn.GP][gpgn.STD][gpgn.POSTERIOR] = {}
        globdat[gpgn.GP][gpgn.SAMPLES] = {}
        globdat[gpgn.GP][gpgn.SAMPLES][gpgn.PRIOR] = {}
        globdat[gpgn.GP][gpgn.SAMPLES][gpgn.POSTERIOR] = {}

        return output

    def shutdown(self, globdat):
        pass

    def take_sample_action(self, samples, globdat):

        # Define a dictionary for the output params
        rng = np.random.default_rng(self._seed)

        # Get the prior and posterior samples
        for model in self.get_relevant_models("GETPRIORSAMPLES", self._models):
            prior_samples, field = model.GETPRIORSAMPLES(globdat, nsamples=self._nsample, rng=rng)
        for model in self.get_relevant_models("KALMANUPDATE", self._models):
            post_samples, field = model.KALMANUPDATE(prior_samples, globdat, rng=rng)

        # Store the prior and posterior samples in globdat
        samples[gpgn.PRIOR][field] = prior_samples
        samples[gpgn.POSTERIOR][field] = post_samples

        # Convert state0 to extForce or vice versa
        self._state0_extforce_conversion(samples[gpgn.PRIOR], field, 'samples')
        self._state0_extforce_conversion(samples[gpgn.POSTERIOR], field, 'samples')

        # Project the samples over the coarse space if needed
        if self._postproject:
            for model in self.get_relevant_models("PROJECTSAMPLES", self._models):
                prior_samples = samples[gpgn.PRIOR][gn.STATE0]
                post_samples = samples[gpgn.POSTERIOR][gn.STATE0]

                prior_samples, post_samples = model.PROJECTSAMPLES(prior_samples, post_samples, globdat)

            # Store the projected samples in globdat
            samples[gpgn.PRIOR][gn.STATE0] = prior_samples
            samples[gpgn.POSTERIOR][gn.STATE0] = post_samples

    def take_sample_field_action(self, samples, globdat):

        # Loop over the different fields
        for name in self._tnames:

            # Get the fine solution table for that field
            globtable = globdat[gn.TABLES][name]

            # Get the tables for both prior and posterior
            for distribution in [gpgn.PRIOR, gpgn.POSTERIOR]:

                # Create a new sub-dictionary
                samples[distribution][name] = {}

                # Add a sample matrix for each component
                for comp in globtable.get_column_names():
                    samples[distribution][name][comp] = np.zeros((globdat[gn.NSET].size(), self._nsample))

                # Go over each state0 sample
                for i, sample in enumerate(samples[distribution][gn.STATE0].T):
                    nodecount = len(globdat[gn.NSET])
                    table = Table(size=nodecount)
                    tbwts = np.zeros(nodecount)

                    # Get the relevant fields
                    for model in self.get_relevant_models("GETTABLE", self._models):
                        table, tbwts = model.GETTABLE(name, table, tbwts, globdat, solution=sample)

                    to_xtable(table)

                    for jcol in range(table.column_count()):
                        values = table.get_col_values(None, jcol)
                        table.set_col_values(None, jcol, values / tbwts)

                    table.to_table()

                    # Add the field to the sample matrices for each component
                    for comp in table.get_column_names():
                        samples[distribution][name][comp][:,i] = table[comp]

    def take_mean_action(self, mean, globdat):

        # Get the prior and posterior mean
        for model in self.get_relevant_models("GETPRIORMEAN", self._models):
            m_prior, field = model.GETPRIORMEAN(globdat)
        for model in self.get_relevant_models("GETPOSTERIORMEAN", self._models):
            m_post, field = model.GETPOSTERIORMEAN(globdat)

        # Store the prior and posterior mean in globdat
        mean[gpgn.PRIOR][field] = m_prior
        mean[gpgn.POSTERIOR][field] = m_post

        # Convert state0 to extForce or vice versa
        self._state0_extforce_conversion(mean[gpgn.PRIOR], field, 'vector')
        self._state0_extforce_conversion(mean[gpgn.POSTERIOR], field, 'vector')

    def take_variance_action(self, cov, globdat):

        # Get the prior and posterior covariance
        for model in self.get_relevant_models("GETPRIORCOVARIANCE", self._models):
            Sigma_prior, field = model.GETPRIORCOVARIANCE(globdat)
        for model in self.get_relevant_models("GETPOSTERIORCOVARIANCE", self._models):
            Sigma_post, field = model.GETPOSTERIORCOVARIANCE(globdat)

        # Store the prior and posterior covariance in globdat
        cov[gpgn.PRIOR][field] = Sigma_prior
        cov[gpgn.POSTERIOR][field] = Sigma_post

        # Convert state0 to extForce or vice versa
        self._state0_extforce_conversion(cov[gpgn.PRIOR], field, 'matrix')
        self._state0_extforce_conversion(cov[gpgn.POSTERIOR], field, 'matrix')

    def _state0_extforce_conversion(self, target, field, shape):
        assert shape in ['vector', 'matrix', 'samples']

        if field == gn.STATE0:
            if shape == 'vector' or shape == 'samples':
                target[gn.EXTFORCE] = self._solver.get_matrix() @ target[gn.STATE0]
            elif shape == 'matrix':
                target[gn.EXTFORCE] = self._solver.get_matrix() @ target[gn.STATE0] @ self._solver.get_matrix()
            else:
                raise ValueError('shape should be "vector", "matrix" or "samples"')
        elif field == gn.EXTFORCE:
            if self._explicit_inverse:
                if shape == 'vector' or shape == 'samples':
                    target[gn.STATE0] = self._Kinv @ target[gn.EXTFORCE]
                elif shape == 'matrix':
                    target[gn.STATE0] = self._Kinv @ target[gn.EXTFORCE] @ self._Kinv
                else:
                    raise ValueError('shape should be "vector", "matrix" or "samples"')
            else:
                self._solver.precon_mode = True
                if shape == 'vector':
                    target[gn.STATE0] = self._solver.solve(target[gn.EXTFORCE])
                elif shape == 'samples':
                    target[gn.STATE0] = self._solver.multisolve(target[gn.EXTFORCE])
                elif shape == 'matrix':
                    target[gn.STATE0] = self._solver.multisolve(self._solver.multisolve(target[gn.EXTFORCE]).T)
                else:
                    raise ValueError('shape should be "vector", "matrix" or "samples"')
                self._solver.precon_mode = False
        else:
            raise ValueError('field should be either STATE0 or EXTFORCE')
