from myjive.names import GlobNames as gn

from myjive.app import ModuleFactory
from myjive.model import ModelFactory

from gpmodel import GPModel
from gpfmodel import GPfModel
from gpenkfmodel import GPEnKFModel

from gpinitmodule import GPInitModule
from gpmodule import GPModule
from gpexactmodule import GPExactModule
from gpsamplermodule import GPSamplerModule


def declare_extra_models(globdat):
    factory = globdat.get(gn.MODELFACTORY, ModelFactory())

    GPModel.declare(factory)
    GPfModel.declare(factory)
    GPEnKFModel.declare(factory)

    globdat[gn.MODELFACTORY] = factory


def declare_extra_modules(globdat):
    factory = globdat.get(gn.MODULEFACTORY, ModuleFactory())

    GPInitModule.declare(factory)
    GPModule.declare(factory)
    GPExactModule.declare(factory)
    GPSamplerModule.declare(factory)

    globdat[gn.MODULEFACTORY] = factory
