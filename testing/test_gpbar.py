import pytest
import os
cwd = os.getcwd()
rootdir = os.path.join(cwd[:cwd.rfind(os.path.sep + "bfem")], "bfem")

import numpy as np

import myjive.util.proputils as pu
from myjive.app import main
from myjive.solver import Constrainer
from myjivex import declare_all as declarex
from declare import declare_extra_models, declare_extra_modules

@pytest.fixture(autouse=True)
def change_test_dir(monkeypatch):
    monkeypatch.chdir(rootdir)
    monkeypatch.chdir('gpexamples/gpbar')

@pytest.fixture
def props():
    return pu.parse_file('2nodebar.pro')

@pytest.mark.rank1
@pytest.mark.bar
@pytest.mark.gp
def test_moments(props):
    extra_declares = [declarex, declare_extra_models, declare_extra_modules]
    globdat = main.jive(props, extra_declares=extra_declares)

    K = globdat['matrix0']
    u = globdat['state0']
    f = globdat['extForce']
    c = globdat['constraints']

    conman = Constrainer(c, K)
    Kc = conman.get_output_matrix()
    fc = conman.get_rhs(f)

    # Check solver solution
    assert np.isclose(Kc @ u, fc).all()

    p25 = (len(u)-1)//4
    mid = (len(u)-1)//2
    p75 = (len(u)-1)//4*3

    mean = globdat['gp']['mean']
    u_prior = mean['prior']['state0']
    f_prior = mean['prior']['extForce']
    u_post = mean['posterior']['state0']
    f_post = mean['posterior']['extForce']

    assert np.isclose(f_prior, 0).all()
    assert np.isclose(u_prior, 0).all()
    # assert np.isclose(max(u_prior), u_prior[mid])
    # assert np.isclose(min(u_prior), 0)

    assert np.isclose(f_post[mid], 0.3952837213234438)
    assert np.isclose(max(f_post), f_post[mid])
    assert np.isclose(min(f_post), 0)
    assert np.isclose(u_post[mid], 2.2429630646764194)
    assert np.isclose(max(u_post), u_post[mid])
    assert np.isclose(min(u_post), 0)

    std = globdat['gp']['std']
    std_u_prior = std['prior']['state0']
    std_f_prior = std['prior']['extForce']
    std_u_post = std['posterior']['state0']
    std_f_post = std['posterior']['extForce']

    pdnoise = 1e-8

    assert np.isclose(std_f_prior[[0,-1]], pdnoise).all()
    assert np.isclose(std_f_prior[1:-1], 0.6747632114003022).all()
    assert np.isclose(std_u_prior[[0,-1]], pdnoise).all()
    assert np.isclose(std_u_prior[mid], 1.0453502237919374)
    assert np.isclose(max(std_u_prior), std_u_prior[mid])
    assert np.isclose(std_u_prior[[p25,p75]], 1.0245974596412912).all()
    assert np.isclose(min(std_u_prior), pdnoise)

    assert np.isclose(std_f_post[[0,-1]], pdnoise).all()
    assert np.isclose(std_f_post[mid], 0.6492419435570032)
    assert np.isclose(std_f_post[[p25,p75]], 0.6401702925012068).all()
    assert np.isclose(std_u_post[[0,-1]], pdnoise).all()
    assert np.isclose(std_u_post[mid], 0.40385470472406826)
    assert np.isclose(std_u_post[[p25,p75]], 0.3225657576789776).all()
    assert np.isclose(min(std_u_post), pdnoise)
