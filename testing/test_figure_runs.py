import pytest
import os

cwd = os.getcwd()
rootdir = os.path.join(cwd[: cwd.rfind(os.path.sep + "bfem")], "bfem")

# some code at the start of each script to suppress matplotlib from showing figures
prefix = ""
prefix += "import matplotlib\n"
prefix += "import warnings\n"
prefix += 'matplotlib.use("agg")\n'
prefix += 'warnings.filterwarnings("ignore", message="Matplotlib is currently using agg, which is a non-GUI backend, so cannot show the figure.")\n'

# some code at the end of each script to suppress matplotlib from showing figures
suffix = ""
suffix += "import matplotlib.pyplot as plt\n"
suffix += "plt.close()\n"

@pytest.mark.figures
@pytest.mark.runs
def test_figure_2(monkeypatch):
    monkeypatch.chdir(rootdir)
    monkeypatch.chdir("figures/figure_2_4_5")

    exec(prefix + open("figure_2.py").read() + suffix)

@pytest.mark.figures
@pytest.mark.runs
def test_figure_3(monkeypatch):
    monkeypatch.chdir(rootdir)
    monkeypatch.chdir("figures/figure_3")

    exec(prefix + open("figure_3.py").read() + suffix)

@pytest.mark.figures
@pytest.mark.runs
def test_figure_4(monkeypatch):
    monkeypatch.chdir(rootdir)
    monkeypatch.chdir("figures/figure_2_4_5")

    exec(prefix + open("figure_4.py").read() + suffix)

@pytest.mark.figures
@pytest.mark.runs
def test_figure_5(monkeypatch):
    monkeypatch.chdir(rootdir)
    monkeypatch.chdir("figures/figure_2_4_5")

    exec(prefix + open("figure_5.py").read() + suffix)
