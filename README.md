# BFEM
Python code related to "A Bayesian Approach to Modeling Finite Element Discretization Error" by Anne Poot, Pierre Kerfriden, Iuri Rocha and Frans van der Meer. The article is currently awaiting publication, but a preprint is available [here](https://arxiv.org/pdf/2306.05993.pdf).

## Getting started
The BFEM code relies on FEM code provided by [MyJive](https://gitlab.tudelft.nl/apoot1/myjive), which can be imported via PyPI (and in the future, anaconda). To avoid needing to add `sys.path.append("..")`-like statements in all examples, you can use the conda develop call as shown below:

```
conda env create -f ENVIRONMENT.yml
conda activate bfem
conda develop /path/to/bfem/
```

This should be sufficient to handle all dependencies of the BFEM code.

## Figures
All figures in the article can be recreated by running the appropriate scripts in the `bfem/figures/` folder. To reproduces figures 2b-2d in the article, simply cd to `bfem/figures/figure-2-4-5/` and run `figure-2.py`. The same applies to the other figures in the paper.

## Examples
All BFEM-related examples are stored in a directory that starts with `gp`.

Examples appearing the article:

- `gptapered`: similar to `gpbar`, but the bar is tapered, and inhomogeneous Dirichlet boundary conditions have been applied
- `gpplate`: plate with a hole, loaded horizontally, using different levels of refinement on different sides of the hole.

Additional examples:

- `gpbar`: 1D bar problem with a point load in the center
- `gpbeam`: 2D simply supported beam loaded by its self-weight.
- `gpcantilever`: 2D cantilever beam loaded by its self-weight.
- `gpensemble`: similar to `gptapered`, but unlike the other examples an ensemble Kalman filter is used instead of an exact Kalman update.
- `gppoisson`: 2D poisson problem, using p-refinement instead of h-refinement like the other examples.
- `gprve`: 2D micromodel of a porous material with a very complicated domain.
- `gpsingularity`: 2D L-shaped beam, which has a singularity in its inner corner.

## GP Modules
- `GPInitModule`: initializes the coarse mesh (child class of `initmodule`)
- `GPModule`: defines basic BFEM actions, and performs configuration (child class of `LinsolveModule`)
- `GPExactModule`: computes the prior and posterior mean and covariances exactly (child class of `GPModule`)
- `GPSamplerModule`: draws samples from the prior and posterior and computes means and covariances from those (child class of `GPModule`)

## GP Models
- `GPModel`: fundamental BFEM model, performing all calculations exactly and making no assumptions on the prior
- `GPfModel`: assumes that the prior can be defined on $`K u`$ (i.e. $`f`$) instead of $`u`$ (child class of `GPModel`)
- `GPEnKFModel`: implements the posterior update using an Ensemble Kalman Filter instead of the exact Kalman update (child class of `GPModel`)
