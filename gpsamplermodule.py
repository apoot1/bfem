import numpy as np

from myjive.names import GlobNames as gn
from names import GPGlobNames as gpgn

from gpmodule import GPModule

class GPSamplerModule(GPModule):

    def run(self, globdat):

        # Run gpmodule (and solvermodule) first
        output = super().run(globdat)

        # Get the sub-dictionaries of the mean, std and samples
        mean = globdat[gpgn.GP][gpgn.MEAN]
        std = globdat[gpgn.GP][gpgn.STD]
        samples = globdat[gpgn.GP][gpgn.SAMPLES]

        # Get the samples
        self.take_sample_action(samples, globdat)

        # Get the fields belonging to the samples
        self.take_sample_field_action(samples, globdat)

        # Compute the prior and posterior mean from the samples
        mean[gpgn.PRIOR][gn.STATE0]       = np.mean(samples[gpgn.PRIOR][gn.STATE0], axis=1)
        mean[gpgn.PRIOR][gn.EXTFORCE]     = np.mean(samples[gpgn.PRIOR][gn.EXTFORCE], axis=1)
        mean[gpgn.POSTERIOR][gn.STATE0]   = np.mean(samples[gpgn.POSTERIOR][gn.STATE0], axis=1)
        mean[gpgn.POSTERIOR][gn.EXTFORCE] = np.mean(samples[gpgn.POSTERIOR][gn.EXTFORCE], axis=1)

        # Compute the prior and posterior standard deviation from the samples
        std[gpgn.PRIOR][gn.STATE0]       = np.std(samples[gpgn.PRIOR][gn.STATE0], axis=1)
        std[gpgn.PRIOR][gn.EXTFORCE]     = np.std(samples[gpgn.PRIOR][gn.EXTFORCE], axis=1)
        std[gpgn.POSTERIOR][gn.STATE0]   = np.std(samples[gpgn.POSTERIOR][gn.STATE0], axis=1)
        std[gpgn.POSTERIOR][gn.EXTFORCE] = np.std(samples[gpgn.POSTERIOR][gn.EXTFORCE], axis=1)

        return output

    def shutdown(self, globdat):
        pass

