import numpy as np

from myjive.names import GlobNames as gn
from names import GPGlobNames as gpgn

from gpmodule import GPModule

class GPExactModule(GPModule):

    @classmethod
    def get_type(cls):
        return "GPSolver"

    def run(self, globdat):

        # Run gpmodule (and solvermodule) first
        output = super().run(globdat)

        # Get the sub-dictionaries of the mean, covariance, std and samples
        mean = globdat[gpgn.GP][gpgn.MEAN]
        cov = globdat[gpgn.GP][gpgn.COVARIANCE]
        std = globdat[gpgn.GP][gpgn.STD]
        samples = globdat[gpgn.GP][gpgn.SAMPLES]

        # Get the mean
        self.take_mean_action(mean, globdat)

        # Get the covariance
        self.take_variance_action(cov, globdat)

        # Compute the prior and posterior standard deviation from the covariance matrix
        std[gpgn.PRIOR][gn.STATE0]       = np.sqrt(cov[gpgn.PRIOR][gn.STATE0].diagonal())
        std[gpgn.PRIOR][gn.EXTFORCE]     = np.sqrt(cov[gpgn.PRIOR][gn.EXTFORCE].diagonal())
        std[gpgn.POSTERIOR][gn.STATE0]   = np.sqrt(cov[gpgn.POSTERIOR][gn.STATE0].diagonal())
        std[gpgn.POSTERIOR][gn.EXTFORCE] = np.sqrt(cov[gpgn.POSTERIOR][gn.EXTFORCE].diagonal())

        # Get the log likelihood and store it in globdat
        for model in self.get_relevant_models("GETLOGLIKELIHOOD", self._models):
            l = model.GETLOGLIKELIHOOD(globdat)
        globdat[gpgn.GP][gpgn.LOGLIKELIHOOD] = l

        # Get the samples
        self.take_sample_action(samples, globdat)

        # Get the fields belonging to the samples
        self.take_sample_field_action(samples, globdat)

        return output

    def shutdown(self, globdat):
        pass
