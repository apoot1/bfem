import numpy as np
import scipy.sparse as spsp
import scipy.sparse.linalg as spspla

from myjive.solver.jit.cholesky import sparse_cholesky
from myjive.names import GlobNames as gn
from gpmodel import GPModel

ENSEMBLE = 'ensemble'
SEED = 'seed'
PRIOR = 'prior'
PREMULTIPLIER = 'premultiplier'
DIAGONALIZED = 'diagonalized'


class GPEnKFModel(GPModel):
    @GPModel.save_config
    def configure(self, globdat, *, prior, ensemble=100, seed=None, **otherprops):

        super().configure(globdat, prior=prior, **otherprops)

        # Get props
        self._nens = ensemble
        self._seed = seed

        self._premultiplier = prior.get(PREMULTIPLIER, None)
        self._diagonalized = prior.get(DIAGONALIZED, False)

    def _configure_prior(self, globdat):

        super()._configure_prior(globdat)

        # Define a dictionary with relevant functions
        eval_dict = self._get_eval_dict(globdat)

        # Get the premultiplier matrix (if necessary)
        if not self._premultiplier is None:
            preK = eval(self._premultiplier, eval_dict)

        # Check if we have a diagonalizable prior
        if self._diagonalized:

            # If so, get the cholesky root from the diagonal
            self._sqrtSigma = spsp.diags(np.sqrt(self._Sigma.sum(axis=1)), format='csr')

        else:

            # If not, do a full Cholesky decomposition (assuming Sigma is a sparse matrix)
            self._sqrtSigma = sparse_cholesky(self._Sigma)

        # Take the GETPRIORSAMPLES action to build the ensemble
        prior_samples, field = self.GETPRIORSAMPLES(globdat, nsamples=self._nens, rng=np.random.default_rng(self._seed))

        # Obtain the ensemble
        self._X = prior_samples

        # Apply the premultiplier to the samples
        for i in range(self._nens):
            self._X[:,i] = spspla.spsolve(preK, self._X[:,i])

        # Now, delete self._Sigma and self._sqrtSigma. We don't need them any more!
        del self._Sigma
        del self._sqrtSigma

        # Get the mean of X, and the deviation from the mean
        self._m = np.mean(self._X, axis=1)
        self._y = self._g - self._H @ self._m
        self._A = self._X - np.tile(self._m, (self._nens,1)).T

        # Compute the product of H and A (which is nobs x nens)
        self._HA = self._H @ self._A

    def _get_eval_dict(self, globdat):

        # Define a dictionary with relevant functions
        eval_dict = {'inv':spspla.inv, 'exp':np.exp, 'norm':np.linalg.norm, 'np':np}
        eval_dict.update(self._hyperparams)

        # Check if we have an SPDE covariance
        if self._prior == 'SPDE':

            nodes = globdat[gn.NSET]
            dofspace = globdat[gn.DOFSPACE]

            if self._rank >= 1:
                x = np.zeros(self._dc)
            if self._rank >= 2:
                y = np.zeros(self._dc)

            for i in range(len(nodes)):
                icoords = nodes[i].get_coords()
                idofs = dofspace.get_dofs([i], dofspace.get_types())

                if self._rank >= 1:
                    x[idofs] = icoords[0]
                if self._rank >= 2:
                    y[idofs] = icoords[1]

            if self._rank >= 1:
                eval_dict['x'] = x
            if self._rank >= 2:
                eval_dict['y'] = y

            # Add the mass and stiffness matrices to the dictionary
            eval_dict['M'] = self._Mc
            eval_dict['K'] = self._Kc
            eval_dict['Phi'] = self._Phi

        return eval_dict

    def _get_prior_covariance(self, globdat):

        ####################
        # PRIOR COVARIANCE #
        ####################

        # Compute the full covariance matrix
        Sigma_prior = self._A @ self._A.T / (self._nens - 1)
        return Sigma_prior, gn.STATE0

    def _apply_covariance_bcs(self, Sigma, globdat):
        Sigmac = Sigma.copy()
        mc = np.zeros(self._dc)

        # Add a tiny noise to ensure Sigma is positive definite rather than semidefinite
        Sigmac += self._pdnoise2 * spsp.identity(self._dc)

        # Check if the boundary condition should be applied directly or via dirichlet BCs
        if self._bctype == 'dirichlet':

            if self._premultiplier != "K":
                raise ValueError('With GPEnKFModel, BCs can only be applied if K is the premultiplier')

            # Split K along boundary and internal nodes
            K_ib = -self._K[:,self._cdofs]
            K_ib[self._cdofs] = spsp.identity(len(self._cdofs))

            # Decouple the bc covariance from the internal nodes
            Sigmac[self._cdofs,:] *= 0.0
            Sigmac[:,self._cdofs] *= 0.0

            # Get a matrix that defines the constraint equations
            conmat = spsp.lil_array((self._dc, len(self._bcgroups)))
            ds = globdat[gn.DOFSPACE]
            for i, (group, dof) in enumerate(zip(self._bcgroups, self._bcdofs)):
                idofs = ds.get_dofs(globdat[gn.NGROUPS][group], [dof])
                conmat[idofs, i] = 1
            conmat = conmat[self._cdofs,:]

            # Get the boundary mean vector
            meanvec = np.array(self._bcmeans)
            mean_bc = conmat @ meanvec

            # Add the boundary mean vector to the prior
            mc += K_ib @ mean_bc

            # Get the boundary covariance matrix
            covmat = spsp.diags(self._bccovs)**2
            Sigma_bc = conmat @ covmat @ conmat.T

            # Recouple the internal nodes based on the boundary covariance matrix
            Sigmac += K_ib @ Sigma_bc @ K_ib.T

        elif self._bctype == 'direct':
            raise ValueError('With GPfModel, BCs cannot be applied directly')

        else:
            raise ValueError('boundary has to be "dirichlet" or "direct"')

        # Add separate boundary noise to ensure positive definiteness
        noisediag = np.zeros(self._dc)
        noisediag[self._cdofs] = self._bcnoise2
        Sigmac += spsp.diags(noisediag)

        return mc, Sigmac


    def _get_Sigma_obs(self):

        if not '_Sigma_obs' in vars(self):
            self._Sigma_obs = self._HA @ self._HA.T / (self._nens-1) + np.identity(self._nobs) * self._noise2

    def _get_sqrtObs(self):

        if not '_sqrtObs' in vars(self):
            self._get_Sigma_obs()
            self._sqrtObs = np.linalg.cholesky(self._Sigma_obs)

    def _get_sqrtSigma(self):

        if not '_sqrtSigma' in vars(self):
            self._sqrtSigma = self._A / np.sqrt(self._nens-1)

    def _premul_Sigma(self, X):

        return self._A @ (self._A.T @ X) / (self._nens-1)

    def _postmul_Sigma(self, X):

        return (X @ self._A) @ self._A.T / (self._nens-1)
