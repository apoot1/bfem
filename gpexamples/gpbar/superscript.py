import matplotlib.pyplot as plt
import numpy as np
from myjive.app import main
import myjive.util.proputils as pu
from declare import declare_extra_models, declare_extra_modules
from myjivex import declare_all as declarex

def mesher_lin(L, n, fname='2nodebar'):
    dx = L / n
    if not '.' in fname:
        fname += '.mesh'

    with open(fname, 'w') as fmesh:
        fmesh.write('nodes (ID, x, [y], [z])\n')
        for i in range(n + 1):
            fmesh.write('%d %f\n' % (i, i * dx))
        fmesh.write('elements (node#1, node#2, [node#3, ...])\n')
        for i in range(n):
            fmesh.write('%d %d\n' % (i, i + 1))

props = pu.parse_file('2nodebar.pro')

L = 10

extra_declares = [declarex, declare_extra_models, declare_extra_modules]
globdat = main.jive(props, extra_declares=extra_declares)
K = globdat['matrix0']
M = globdat['matrix2']
u = globdat['state0']

mean = globdat['gp']['mean']
u_prior = mean['prior']['state0']
f_prior = mean['prior']['extForce']
u_post = mean['posterior']['state0']
f_post = mean['posterior']['extForce']

std = globdat['gp']['std']
std_u_prior = std['prior']['state0']
std_f_prior = std['prior']['extForce']
std_u_post = std['posterior']['state0']
std_f_post = std['posterior']['extForce']

samples = globdat['gp']['samples']
samples_u_prior = samples['prior']['state0']
samples_f_prior = samples['prior']['extForce']
samples_u_post = samples['posterior']['state0']
samples_f_post = samples['posterior']['extForce']

x = np.linspace(0, L, len(u))

fig, [[ax1,ax2],[ax3,ax4]] = plt.subplots(2,2)
fig.suptitle(r'1D bar test case')
plt.tight_layout()
ax1.set_title(r'prior on $f$')
ax1.fill_between(x, f_prior - 2*std_f_prior, f_prior + 2*std_f_prior, alpha=0.3)
ax1.plot(x, samples_f_prior, color='gray', linewidth=0.2)
ax1.plot(x, f_prior)
ax2.set_title(r'prior on $u$')
ax2.fill_between(x, u_prior - 2*std_u_prior, u_prior + 2*std_u_prior, alpha=0.3)
ax2.plot(x, samples_u_prior, color='gray', linewidth=0.2)
ax2.plot(x, u_prior)
ax3.set_title(r'posterior on $f$')
ax3.fill_between(x, f_post - 2*std_f_post, f_post + 2*std_f_post, alpha=0.3)
ax3.plot(x, samples_f_post, color='gray', linewidth=0.2)
ax3.plot(x, f_post)
ax3.plot(x[1:-1], (K @ u)[1:-1])
ax4.set_title(r'posterior on $u$')
ax4.fill_between(x, u_post - 2*std_u_post, u_post + 2*std_u_post, alpha=0.3)
ax4.plot(x, samples_u_post, color='gray', linewidth=0.2)
ax4.plot(x, u_post)
ax4.plot(x, u)
plt.show()
