import numpy as np
from myjive.app import main
import myjive.util.proputils as pu
from declare import declare_extra_models, declare_extra_modules
from myjivex.util import QuickViewer
from copy import deepcopy
from myjivex import declare_all as declarex

props = pu.parse_file('plate.pro')

props_c = {}
props_c['init'] = deepcopy(props['gpinit'])
props_c['init']['type'] = 'Init'
props_c['solver'] = deepcopy(props['gpsolver'])
props_c['solver']['type'] = 'Linsolve'
props_c['solver'].pop('nsample')
props_c['solver'].pop('seed')
props_c['model'] = deepcopy(props['model'])
props_c['model']['models'] = [ "solid", "load", "diri" ]
props_c['init']['mesh']['file'] = 'meshes/plate_r0.msh'

extra_declares = [declarex, declare_extra_models, declare_extra_modules]
globdat_c = main.jive(props_c, extra_declares=extra_declares)
u_coarse = globdat_c['state0']
strain_xx_c = globdat_c['tables']['strain']['xx']
strain_yy_c = globdat_c['tables']['strain']['yy']
strain_c = np.append(strain_xx_c, strain_yy_c)

globdat = main.jive(props, extra_declares=extra_declares)
K = globdat['matrix0']
M = globdat['matrix2']
f = globdat['extForce']
u = globdat['state0']

strain_xx = globdat['tables']['strain']['xx']
strain_yy = globdat['tables']['strain']['yy']
strain = np.append(strain_xx, strain_yy)

loglikelihood = globdat['gp']['logLikelihood']

mean = globdat['gp']['mean']
u_prior = mean['prior']['state0']
f_prior = mean['prior']['extForce']
u_post = mean['posterior']['state0']
f_post = mean['posterior']['extForce']

std = globdat['gp']['std']
std_u_prior = std['prior']['state0']
std_f_prior = std['prior']['extForce']
std_u_post = std['posterior']['state0']
std_f_post = std['posterior']['extForce']

samples = globdat['gp']['samples']
samples_u_prior = samples['prior']['state0']
samples_f_prior = samples['prior']['extForce']
samples_u_post = samples['posterior']['state0']
samples_f_post = samples['posterior']['extForce']

samples_eps_prior = samples['prior']['strain']
std_eps_xx_prior = np.std(samples_eps_prior['xx'], axis=1)
std_eps_yy_prior = np.std(samples_eps_prior['yy'], axis=1)

samples_eps_post = samples['posterior']['strain']
std_eps_xx_post = np.std(samples_eps_post['xx'], axis=1)
std_eps_yy_post = np.std(samples_eps_post['yy'], axis=1)

eps_xx_post = np.mean(samples_eps_post['xx'], axis=1)
eps_yy_post = np.mean(samples_eps_post['yy'], axis=1)

Phi = globdat['Phi']

err = abs(u - Phi @ u_coarse)
err_grad = abs(strain - Phi @ strain_c)

QuickViewer(u, globdat, comp=0, dpi=600, title=r'displacement ($u_x$)', fname='img/core-plots/state0-x.png')
QuickViewer(strain_xx, globdat, dpi=600, title=r'strain ($\varepsilon_{xx}$)', fname='img/core-plots/strain-xx.png')

QuickViewer(err, globdat, comp=0, dpi=600, title=r'displacement error ($\Delta_{u_x}$)', fname='img/core-plots/error_state0-x.png')
QuickViewer(err_grad, globdat, comp=0, dpi=600, title=r'strain error ($\Delta_{\varepsilon_{xx}}$)', fname='img/core-plots/error_strain-xx.png')

QuickViewer(u_post, globdat, comp=0, dpi=600, title=r'posterior mean displacement ($m^*$)', fname='img/mean_state0-x_posterior.png')

QuickViewer(std_u_prior, globdat, comp=0, dpi=600, title=r'prior std displacement ($\sigma_{u_{x}}$)', fname='img/std_state0-x_prior.png')
QuickViewer(std_u_post, globdat, comp=0, dpi=600, title=r'posterior std displacement ($\sigma^*_{u_{x}}$)', fname='img/std_state0-x_posterior.png')
QuickViewer(std_eps_xx_prior, globdat, dpi=600, title=r'prior std strain ($\sigma_{\varepsilon_{xx}}$)', fname='img/std_strain-xx_prior.png')
QuickViewer(std_eps_xx_post, globdat, dpi=600, title=r'posterior std strain ($\sigma^*_{\varepsilon_{xx}}$)', fname='img/std_strain-xx_posterior.png')
