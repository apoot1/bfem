import numpy as np
from myjive.app import main
import myjive.util.proputils as pu
from declare import declare_extra_models, declare_extra_modules
from myjivex.util import QuickViewer
from myjivex import declare_all as declarex

props = pu.parse_file('singularity.pro')

gamma_rel = np.logspace(-4,4,9)
epsilon_rel = np.logspace(-4,4,9)

for e_rel in epsilon_rel:
    for g_rel in gamma_rel:
        alpha = 1.
        gamma = alpha * g_rel
        epsilon = alpha * e_rel

        print('Running the model for gamma={:.1e}, epsilon={:.1e}'.format(gamma, epsilon))
        props['model']['gp']['prior']['func'] = 'alpha**2 * M + gamma**2 * F'
        props['model']['gp']['prior']['hyperparams']['alpha'] = str(alpha)
        props['model']['gp']['prior']['hyperparams']['gamma'] = str(gamma)
        props['model']['gp']['obsNoise'] = epsilon

        extra_declares = [declarex, declare_extra_models, declare_extra_modules]
        globdat = main.jive(props, extra_declares=extra_declares)

        mean = globdat['gp']['mean']
        u_prior = mean['prior']['state0']
        f_prior = mean['prior']['extForce']
        u_post = mean['posterior']['state0']
        f_post = mean['posterior']['extForce']

        std = globdat['gp']['std']
        std_u_prior = std['prior']['state0']
        std_f_prior = std['prior']['extForce']
        std_u_post = std['posterior']['state0']
        std_f_post = std['posterior']['extForce']

        samples = globdat['gp']['samples']

        samples_eps_prior = samples['prior']['strain']
        std_eps_xx_prior = np.std(samples_eps_prior['xx'], axis=1)
        std_eps_yy_prior = np.std(samples_eps_prior['yy'], axis=1)

        samples_eps_post = samples['posterior']['strain']
        std_eps_xx_post = np.std(samples_eps_post['xx'], axis=1)
        std_eps_yy_post = np.std(samples_eps_post['yy'], axis=1)

        fname = 'img/systematic-plots/alpha-{:.0e}_gamma-{:.0e}_epsilon-{:.0e}_'.format(alpha, gamma, epsilon)

        QuickViewer(u_post, globdat, comp=0, pdf=True, colorbar=False, figsize=(6,6), fname=fname+'mean_state0-x_posterior.pdf')
        QuickViewer(u_post, globdat, comp=1, pdf=True, colorbar=False, figsize=(6,6), fname=fname+'mean_state0-y_posterior.pdf')

        QuickViewer(std_u_prior, globdat, comp=0, pdf=True, colorbar=False, figsize=(6,6), fname=fname+'std_state0-x_prior.pdf')
        QuickViewer(std_u_prior, globdat, comp=1, pdf=True, colorbar=False, figsize=(6,6), fname=fname+'std_state0-y_prior.pdf')
        QuickViewer(std_u_post, globdat, comp=0, pdf=True, colorbar=False, figsize=(6,6), fname=fname+'std_state0-x_posterior.pdf')
        QuickViewer(std_u_post, globdat, comp=1, pdf=True, colorbar=False, figsize=(6,6), fname=fname+'std_state0-y_posterior.pdf')

        QuickViewer(std_eps_xx_prior, globdat, pdf=True, colorbar=False, figsize=(6,6), fname=fname+'std_strain-xx_prior.pdf')
        QuickViewer(std_eps_yy_prior, globdat, pdf=True, colorbar=False, figsize=(6,6), fname=fname+'std_strain-yy_prior.pdf')
        QuickViewer(std_eps_xx_post, globdat, pdf=True, colorbar=False, figsize=(6,6), fname=fname+'std_strain-xx_posterior.pdf')
        QuickViewer(std_eps_yy_post, globdat, pdf=True, colorbar=False, figsize=(6,6), fname=fname+'std_strain-yy_posterior.pdf')
