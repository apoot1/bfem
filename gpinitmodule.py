from myjive.names import GlobNames as gn
from names import GPGlobNames as gpgn
from myjive.app import InitModule
from myjive.fem import DofSpace
from myjive.util.proputils import check_dict

COARSEMESH = 'coarseMesh'
TYPE = 'type'
FILE = 'file'

class GPInitModule(InitModule):
    @InitModule.save_config
    def configure(self, globdat, *, coarseMesh, **otherprops):
        super().configure(globdat, **otherprops)

        check_dict(self, coarseMesh, [TYPE, FILE])
        self._coarsemeshprops = coarseMesh

    def init(self, globdat, *, modelprops):
        # Create a separate dictionary to store the coarse mesh properties
        globdat[gpgn.COARSEMESH] = {}

        # Initialize the node/elemenet group dictionaries
        globdat[gpgn.COARSEMESH][gn.NGROUPS] = {}
        globdat[gpgn.COARSEMESH][gn.EGROUPS] = {}

        # Initialize DofSpace
        print('GPInitModule: Creating DofSpace...')
        globdat[gpgn.COARSEMESH][gn.DOFSPACE] = DofSpace()

        # Read mesh
        if 'gmsh' in self._coarsemeshprops[TYPE]:
            self._read_gmsh(self._coarsemeshprops[FILE], globdat[gpgn.COARSEMESH])
        elif 'manual' in self._coarsemeshprops[TYPE]:
            self._read_mesh(self._coarsemeshprops[FILE], globdat[gpgn.COARSEMESH])
        elif 'meshio' in self._coarsemeshprops[TYPE]:
            self._read_meshio(self._coarsemeshprops[FILE], globdat[gpgn.COARSEMESH])
        elif 'geo' in self._coarsemeshprops[TYPE]:
            self._read_geo(self._coarsemeshprops[FILE], globdat[gpgn.COARSEMESH])
        else:
            raise KeyError('GPInitModule: Mesh input type unknown')

        # Create node groups in the coarse mesh
        if self._node_groups is not None:
            print('GPInitModule: Creating node groups...')
            self._create_ngroups(self._node_groups, globdat[gpgn.COARSEMESH], **self._groupprops)

        # Create element groups in the coarse mesh
        if self._elem_groups is not None:
            print('GPInitModule: Creating element groups...')
            self._create_egroups(self._elem_groups, globdat[gpgn.COARSEMESH])

        # Initialize initmodule
        # Note that this needs to happen last, otherwise gpmodel.configure() will not have the coarse mesh available
        super().init(globdat, modelprops=modelprops)

    def run(self, globdat):
        return 'ok'

    def shutdown(self, globdat):
        pass
